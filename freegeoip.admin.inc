<?php

/**
 * @file
 * Defines forms for configuration.
 */

/**
 * Configuration form for changing the server and default country.
 */
function freegeoip_form($form, &$form_state) {
  $form = array();
  $form['description'] = array(
    '#type' => 'item',
    '#title' => t('Settings form to set the server location of freegeoip.net Server.'),
  );
  $form['freegeoip_url'] = array(
    '#type' => 'textfield',
    '#title' => t('FreeGeoIP URL'),
    '#description' => t('Enter the freegeoip.net server URL, format : http://{I.P}:{port}. Default is http://freegeoip.net'),
    '#default_value' => variable_get('freegeoip_url', 'http://freegeoip.net'),
    '#required' => TRUE,
  );
  $form['freegeoip_default_country'] = array(
    '#type' => 'textfield',
    '#title' => t('Default Country'),
    '#description' => t('Enter the default country'),
    '#default_value' => variable_get('freegeoip_default_country', 'india'),
    '#required' => TRUE,
  );
  return system_settings_form($form);
}

/**
 * Check if the entered URL is actually a valid freegeoip.net server.
 */
function freegeoip_form_validate($form, &$form_state) {
  $user_ip = freegeoip_get_user_ip();

  $test = freegeoip_get_geoip_data($form_state['values']['freegeoip_url'] . '/json/' . $user_ip);

  if (is_object($test)) {
    drupal_set_message("Connection to " . $form_state['values']['freegeoip_url'] . ' successful.');
  }
  else {
    form_set_error('geoip_url', 'Error connecting to ' . $form_state['values']['geoip_url'] . '. Please check if it is a valid freegeoip.net server.');
  }
}
